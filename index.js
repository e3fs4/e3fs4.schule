import express from 'express';
import * as path from 'path';
import dotenv from 'dotenv';

// This is important for the .env Configuration file!
dotenv.config();

const __dirname = path.resolve();
const ip = process.env.IP;
const port = process.env.PORT;

const app = express();

app.use(express.static(path.join(__dirname, '/src')));

app.get('/', async function(req, res) {
    res.sendFile(path.join(__dirname, '/src/main.html'));
});

app.get('/coming-soon', async function(req, res) {
    res.sendFile(path.join(__dirname, '/src/coming-soon.html'))
})

app.listen(port, ip);
console.log(`Server running at ${ip}:${port}`);